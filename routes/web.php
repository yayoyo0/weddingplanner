<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Admin routes
Route::get('/', 'Admin\AdminController@index');
Route::get('/planner/{id}', 'Admin\AdminController@show');
Route::post('/planner/{id}', 'Admin\AdminController@store');
Route::get('/planner/{id}/delete', 'Admin\AdminController@destroy');
Route::post('/planners/store', 'Admin\AdminController@store');

//Planner routes
Route::get('/weddings', 'Admin\WeddingController@index');
Route::post('/wedding/store', 'Admin\WeddingController@store');
Route::post('/wedding/{id}/delete', 'Admin\WeddingController@destroy');

// Auth routes
Route::get('logout', array('uses' => 'Auth\LoginController@logout'));
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
