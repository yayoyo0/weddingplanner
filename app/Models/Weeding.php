<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weeding extends Model
{
    //
    public function createdBy()
    {
      return $this->hasOne('App\User','id','created_by');
    }
}
