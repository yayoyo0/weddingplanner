<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PlannerMiddleware
{
  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @return mixed
  */
  public function handle($request, Closure $next)
  {
    if (Auth::check())
    {
      $user = Auth::user();
      if($user->hasAnyRole(['planner','admin']))
      {
        return $next($request);
      }
    }
    else
    {
      return redirect('/login');
    }
  }
}
