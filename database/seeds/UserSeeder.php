<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Heriberto",
            'email' => 'super@weedingplanner.com',
            'password' => bcrypt('administrador123'),
        ]);

        $role = Role::create(['name' => 'admin']);
        $role = Role::create(['name' => 'planner']);
        $user = User::find(1);
        $user->assignRole('admin');
    }
}
