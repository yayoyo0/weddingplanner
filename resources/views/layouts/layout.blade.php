<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
  <title>Weeding Planner | @yield('title') </title>
  <!-- Bootstrap Core CSS -->
  <link href="../assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
  <!-- This page CSS -->
  <!-- chartist CSS -->
  <link href="../assets/node_modules/morrisjs/morris.css" rel="stylesheet">
  <!--c3 CSS -->
  <link href="../assets/node_modules/c3-master/c3.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="css/style.css" rel="stylesheet">
  <!-- Dashboard 1 Page CSS -->
  <link href="css/pages/dashboard1.css" rel="stylesheet">
  <!-- You can change the theme colors from here -->
  <link href="css/colors/default.css" id="theme" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="loader">
      <div class="loader__figure"></div>
      <p class="loader__label">Wedding Planner</p>
    </div>
  </div>
  <div id="main-wrapper">
    <header class="topbar">
      <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">
            W P
          </a>
        </div>
        <div class="navbar-collapse">
          <!-- <ul class="navbar-nav mr-auto">
          <li class="nav-item">
          <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="fa fa-bars"></i></a>
        </li>
      </ul> -->
      <ul class="navbar-nav my-lg-0">
        <li class="nav-item dropdown u-pro">
          <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="../assets/images/users/1.jpg" alt="user" class="" />
            <span class="hidden-md-down">{{Auth::user()->name}}</span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="/logout">Cerrar sesión</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>

<aside class="left-sidebar">
  <!-- Sidebar scroll-->
  <div class="scroll-sidebar">
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
      <ul id="sidebarnav">
        @hasrole('admin')
        <li>
          <a class="waves-effect waves-dark" href="/" aria-expanded="false">
            <i class="fa fa-user-circle-o"></i><span class="hide-menu">Usuarios</span>
          </a>
        </li>
        @endhasrole
        <li>
          <a class="waves-effect waves-dark" href="/weddings" aria-expanded="false">
            <i class="fa fa-black-tie" aria-hidden="true"></i><span class="hide-menu">Bodas</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</aside>
<div class="page-wrapper">
  @yield("content")
</div>
<footer class="footer"> © 2018 Wedding Planner</footer>

</div>
</div>
<script src="../assets/node_modules/jquery/jquery.min.js"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="../assets/node_modules/bootstrap/js/popper.min.js"></script>
<script src="../assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="js/perfect-scrollbar.jquery.min.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!--Menu sidebar -->
<script src="js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="js/custom.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.js"></script>

@if(Session::has('msg'))
<script>
toastr['success']('{{Session::get("msg")}}')
</script>
@endif
@if(Session::has('error'))
<script>
toastr['error']('{{Session::get("error")}}')
</script>
@endif

<!--morris JavaScript -->
<!-- <script src="../assets/node_modules/raphael/raphael-min.js"></script>
<script src="../assets/node_modules/morrisjs/morris.min.js"></script>
<script src="../assets/node_modules/d3/d3.min.js"></script>
<script src="../assets/node_modules/c3-master/c3.min.js"></script> -->
<!-- Chart JS -->
<!-- <script src="js/dashboard1.js"></script> -->
</body>

</html>
