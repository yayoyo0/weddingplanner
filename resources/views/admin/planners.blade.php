@extends('layouts.layout')
@section('title', 'Planners')


@section('content')
<!-- Modal -->
<div id="newPlanner" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Agregar un nuevo Planner</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form class="form" action="planners/store" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nombre</label>
                <input type="text" name="name" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" required>
              </div>
            </div>
          </div>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> -->
          <input type="submit" class="pull-right btn btn-info" value="Guardar">
        </form>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-9 align-self-center">
      <h3 class="text-themecolor">Planners</h3>
    </div>
    <div class="col-md-2">
      <button class="btn btn-warning" data-toggle="modal" data-target="#newPlanner">Crear nuevo planner</button>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="d-flex">
            <div>
              <h5 class="card-title">Planners</h5>
            </div>
          </div>
          <div class="table-responsive m-t-20 no-wrap">
            <table class="table vm no-th-brd pro-of-month">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <td style="width:50px;">{{$user->id}}</td>
                  <td>
                    <h6>{{$user->name}}</h6>
                    <!-- <small class="text-muted">Web Designer</small> -->
                  </td>
                  <td>{{$user->email}}</td>
                  <td>
                    <a href="/planner/show" class="btn btn-info">Editar</a>
                    <a href="/planner/{{$user->id}}/delete"class="btn btn-danger">Eliminar</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
