@extends('layouts.layout')
@section('title', 'Planners')

@section('content')
<!-- Modal -->
<div id="newPlanner" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Agregar una boda</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form class="form" action="wedding/store" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nombre completo</label>
                <input type="text" name="name" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Teléfono novia</label>
                <input type="text" name="phone_wife" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Teléfono novio</label>
                <input type="text" name="phone_husband" class="form-control" required>
              </div>
            </div>
          </div>
          <input type="submit" class="pull-right btn btn-info" value="Guardar">
        </form>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-9 align-self-center">
      <h3 class="text-themecolor">Bodas</h3>
    </div>
    <div class="col-md-2">
      <button class="btn btn-warning" data-toggle="modal" data-target="#newPlanner">Crear nueva boda</button>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="d-flex">
            <div>
              <h5 class="card-title">Bodas</h5>
            </div>
          </div>
          <div class="table-responsive m-t-20 no-wrap">
            <table class="table vm no-th-brd pro-of-month">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre completo</th>
                  <th>Email</th>
                  <th>Teléfono novia</th>
                  <th>Teléfono novio</th>
                  @hasrole('admin')
                    <th>Creada por</th>
                  @endhasrole
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($weddings as $wedding)
                <tr>
                  <td style="width:50px;">{{$wedding->id}}</td>
                  <td>
                    <h6>{{$wedding->full_name}}</h6>
                  </td>
                  <td>{{$wedding->email}}</td>
                  <td>{{$wedding->wife_phone}}</td>
                  <td>{{$wedding->husband_phone}}</td>
                  @hasrole('admin')
                  <td>{{$wedding->createdBy->name}}</td>
                  @endhasrole
                  <td>
                    <a href="/wedding/show" class="btn btn-info">Editar</a>
                    <a href="/wedding/{{$wedding->id}}/delete"class="btn btn-danger">Eliminar</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
